/*
 * Copyright (c) 2018 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
#include <QTest>
#include "translatedcountrylist.h"

class SimpleXmlParserTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testParseMinimalDanish();
    void testBadXml();
    void testNonExisting();
    void testRecursive();
};

void SimpleXmlParserTest::testNonExisting()
{
    auto parsed = TranslatedCountries::parseFilePath(QString());
    QVERIFY(parsed.isEmpty());
}

void SimpleXmlParserTest::testBadXml()
{
    auto parsed = TranslatedCountries::parseFilePath(QFINDTESTDATA("simplexml.cpp"));
    QVERIFY(parsed.isEmpty());
}


void SimpleXmlParserTest::testParseMinimalDanish() {
    QString path = QFINDTESTDATA("da-minimal-for-tests.xml");
    auto parsed = TranslatedCountries::parseFilePath(path);

    QCOMPARE(parsed.size(), 4);
    QCOMPARE(parsed.value("Østrig"), QString("at"));
    QCOMPARE(parsed.value("Ukendt område"), QString("zz"));
    QCOMPARE(parsed.value("Den Demokratiske Republik Congo (DRC)"), QString("cd"));
    QCOMPARE(parsed.value("Congo-Kinshasa"), QString("cd"));
}

void SimpleXmlParserTest::testRecursive()
{
    QString path = QFINDTESTDATA(".");
    auto parsed = TranslatedCountries::parseFilesRecursive(path);

    QCOMPARE(parsed.size(), 4);
    QCOMPARE(parsed.value("Østrig"), QString("at"));
    QCOMPARE(parsed.value("Ukendt område"), QString("zz"));
    QCOMPARE(parsed.value("Den Demokratiske Republik Congo (DRC)"), QString("cd"));
    QCOMPARE(parsed.value("Congo-Kinshasa"), QString("cd"));
}


QTEST_APPLESS_MAIN(SimpleXmlParserTest)

#include "simplexml.moc"
