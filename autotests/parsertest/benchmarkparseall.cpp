/*
 * Copyright (c) 2018 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *


 EXCEPT the content of the parseOldFile function

     This file is part of the KContacts framework.
    Copyright (c) 2001 Cornelius Schumacher <schumacher@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.


 */

#include "translatedcountrylist.h"
#include <QTest>
#include <QDebug>
#include <QStandardPaths>

class Benchmark : public QObject {
    Q_OBJECT

    static QMap<QString,QString> parseOldFile(const QString& string);
    QString m_oldFile;
    QString m_unicodeDir;
public:
    Benchmark() : m_oldFile(QStandardPaths::locate(QStandardPaths::GenericDataLocation, QStringLiteral("kf5/kcontacts/countrytransl.map"))), m_unicodeDir(QStringLiteral("/usr/share/unicode/cldr/common/main")) {
    }
    void initTestCase() {
        QVERIFY(!m_oldFile.isEmpty());
        QVERIFY(QFile::exists(m_oldFile));
        QVERIFY(QFile::exists(m_unicodeDir));
    }
private Q_SLOTS:
    void testBenchmarkParseAll();
    void testBenchmarkOldWay();
    void testOldVsNewDataKeys();
    void testOldVsNewDataValues();
};

void Benchmark::testBenchmarkParseAll()
{
    TranslatedCountries::TranslationCountryMap result;
    QBENCHMARK {
        result = TranslatedCountries::parseFilesRecursive(m_unicodeDir);
    }
    QVERIFY(result.size() > 5);
}


// code lifted from kcontacts/src/address.cpp
QMap<QString, QString> Benchmark::parseOldFile(const QString& mapfile)
{
    QMap<QString,QString> map;

    QFile file(mapfile);
    QSet<QString> ancient = { "an", "C", "yu", "tp"}; // files found in old data, but are either "C" or renamed / split countries
    if (file.open(QIODevice::ReadOnly)) {
        QTextStream s(&file);
        QString strbuf = s.readLine();
        while (!strbuf.isEmpty()) {
            QStringList countryInfo = strbuf.split(QLatin1Char('\t'), QString::KeepEmptyParts);

            if (!countryInfo[0].isEmpty() && !ancient.contains(countryInfo[1])) {
                map.insert(countryInfo[0], countryInfo[1]);
            }
            strbuf = s.readLine();
        }
        file.close();
    }
    return map;
}

void Benchmark::testBenchmarkOldWay()
{
    QMap<QString,QString> result;
    QBENCHMARK {
        result = parseOldFile(m_oldFile);
    }
    QVERIFY(result.size() > 5);
}

void Benchmark::testOldVsNewDataKeys() {
    QString mapfile = QStandardPaths::locate(QStandardPaths::GenericDataLocation, QStringLiteral("kf5/kcontacts/countrytransl.map"));
    auto oldData = parseOldFile(m_oldFile);
    auto newData = TranslatedCountries::parseFilesRecursive(m_unicodeDir);
    // is old a subset of new?
    auto olddatakeys = oldData.keys().toSet();
    auto newdatakeys = newData.keys().toSet();


    auto onlyinnew = newdatakeys;
    onlyinnew.subtract(olddatakeys);
    auto onlyinold = olddatakeys;
    onlyinold.subtract(newdatakeys);

    auto common = newdatakeys;
    common.intersect(olddatakeys);

    QVector<QString> different;
    for(const QString& key : qAsConst(common)) {
        QString oldd = oldData[key];
        QString newd = newData[key];
        if (oldd != newd) {
            QString log = QString("%1 %2 %3").arg(key).arg(oldd).arg(newd);
            qDebug() << log;
            different << log;
        }
    }
    qDebug() << "Only in old:" << onlyinold.size();
    qDebug() << "only in new:" << onlyinnew.size();
    qDebug() << "common: " << common.size();
    qDebug() << "different values: " << different.size();
}

void Benchmark::testOldVsNewDataValues() {
    QString mapfile = QStandardPaths::locate(QStandardPaths::GenericDataLocation, QStringLiteral("kf5/kcontacts/countrytransl.map"));
    auto oldData = parseOldFile(m_oldFile);
    auto newData = TranslatedCountries::parseFilesRecursive(m_unicodeDir);
    // is old a subset of new?
    auto olddatavalues = oldData.values().toSet();
    auto newdatavalues = newData.values().toSet();

    auto onlyinnew = newdatavalues;
    onlyinnew.subtract(olddatavalues);
    auto onlyinold = olddatavalues;
    onlyinold.subtract(newdatavalues);

    auto common = newdatavalues;
    common.intersect(olddatavalues);

    for(const auto& code : qAsConst(onlyinold)) {
        qDebug() << code;
    }

    qDebug() << "Only in old:" << onlyinold.size();
    qDebug() << "only in new:" << onlyinnew.size();
    qDebug() << "common: " << common.size();

    QCOMPARE(onlyinold.size(),0);
}

QTEST_APPLESS_MAIN(Benchmark)
#include "benchmarkparseall.moc"
