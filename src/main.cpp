/*
 * Copyright (c) 2018 Sune Vuorela <sune@vuorela.dk>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QFile>
#include "translatedcountrylist.h"


int main(int argc, char** argv) {
    QCoreApplication app(argc,argv);
    app.setApplicationName("generate country name map");
    app.setApplicationVersion("0.1");

    QCommandLineParser p;
    QCommandLineOption sourceDir(QStringList() << "sourceDir" << "s", "Location of CLDR data", "cldrdata", "/usr/share/unicode/cldr/common/main/");
    p.addOption(sourceDir);
    QCommandLineOption outputFile(QStringList() << "outputFile" << "o", "Output file", "output file", "countrytransl.map");
    p.addOption(outputFile);
    p.addHelpOption();
    p.addVersionOption();

    p.process(app);

    if (!p.isSet(sourceDir)) {
        p.showHelp(0);
    }
    if (!p.isSet(outputFile)) {
        p.showHelp(0);
    }


    QString sourceDirPath = p.value(sourceDir);
    QString outputFilePath = p.value(outputFile);

    QFile f(outputFilePath);
    bool success = f.open(QIODevice::WriteOnly | QIODevice::Truncate);
    if (!success) {
        qFatal(QString("failed to open file: " + f.errorString()).toUtf8());
    }

    const QHash<QString,QString> parsedList = TranslatedCountries::parseFilesRecursive(sourceDirPath);

    std::vector<std::pair<QString,QString>> processedList;
    for(auto it = parsedList.keyValueBegin() ; it != parsedList.keyValueEnd() ; it++) {
        processedList.push_back(*it);
    }
    std::sort(processedList.begin(), processedList.end(), [] (const auto& a, const auto& b) {
        return a.second < b.second;
    });

    for(const auto& pair :  processedList) {
        f.write(pair.first.toUtf8());
        f.write("\t");
        f.write(pair.second.toUtf8());
        f.write("\n");
    }
    f.close();


}
